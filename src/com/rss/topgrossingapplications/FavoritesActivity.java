package com.rss.topgrossingapplications;

import java.util.List;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class FavoritesActivity extends ListActivity implements Runnable {

	List<Entry> entries;
	FavoritesListAdapter adapter;
	Handler handler;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		adapter = new FavoritesListAdapter();
		adapter.setDataSet(entries);
		ListView listView = getListView();
		listView.setAdapter(adapter);
		handler = new Handler();
		new Thread(this).start();
		registerForContextMenu(listView);

		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				FavoritesListAdapter adapter = (FavoritesListAdapter) parent
						.getAdapter();
				Entry entry = (Entry) adapter.getItem(position);
				Intent i = new Intent(getApplicationContext(),
						EntryActivity.class);
				i.putExtra(MainActivity.ENTRY_ITEM, entry);
				startActivity(i);
			}
		});

	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
		Entry entry = (Entry) getListView().getItemAtPosition(info.position);
		menu.setHeaderTitle(entry.getTitle());
		getMenuInflater().inflate(R.menu.fav_context_menu, menu);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {

		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
				.getMenuInfo();
		Entry entry = (Entry) getListView().getItemAtPosition(info.position);

		switch (item.getItemId()) {
		case R.id.action_remove:

			class removeDialog extends AlertDialog.Builder {
				public removeDialog(Context arg0, final Entry e) {
					super(arg0);
					setIcon(android.R.drawable.ic_dialog_alert)
							.setTitle(R.string.remove)
							.setMessage(R.string.verify)
							.setPositiveButton(R.string.yes,
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog,
												int which) {
											removeFavorite(e);
										}
									}).setNegativeButton(R.string.no, null);
				}
			}
			new removeDialog(this, entry).show();

			return true;
		default:
			break;
		}

		return super.onContextItemSelected(item);
	}

	@Override
	public void run() {
		FavoritesDBHelper db = new FavoritesDBHelper(getApplicationContext());
		entries = db.getFavorites();
		adapter.setDataSet(entries);
		db.closeDB();

		handler.post(new Runnable() {

			@Override
			public void run() {
				adapter.notifyDataSetChanged();
			}
		});
		
	}

	private class FavoritesListAdapter extends BaseAdapter {
		private List<Entry> entries;

		/**
		 * @param entryList
		 *            - a list of entries to use as the underlying data set
		 */
		public void setDataSet(List<Entry> entryList) {
			this.entries = entryList;
		}

		@Override
		public int getCount() {
			if (entries != null)
				return this.entries.size();
			return 0;
		}

		@Override
		public Object getItem(int position) {
			if (entries != null)
				if (position < this.entries.size())
					return this.entries.get(position);
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (entries == null)
				return null;
			RelativeLayout relativeLayout;
			if (convertView == null) { // if it's not recycled, initialize some
										// attributes
				relativeLayout = (RelativeLayout) getLayoutInflater().inflate(
						R.layout.favorites_list_item, null);
			} else {
				relativeLayout = (RelativeLayout) convertView;
			}

			ImageView imageView = (ImageView) relativeLayout
					.findViewById(R.id.list_iconView);
			TextView textView = (TextView) relativeLayout
					.findViewById(R.id.list_titleView);

			Entry entry = (Entry) getItem(position);

			Bitmap bitmap = entry.getIcon();
			
			if (bitmap == null)
				new DownloadImageTask(imageView);
			else
				imageView.setImageBitmap(bitmap);
			textView.setText(entry.getTitle());

			return relativeLayout;
		}
	}

	/**
	 * Tries to add a provided Entry as a favorite (in a background thread)<br>
	 * Toast will appear saying whether or not it was added successfully
	 */
	private void removeFavorite(final Entry e) {
		entries.remove(e);
		adapter.notifyDataSetChanged();
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {

				FavoritesDBHelper db = new FavoritesDBHelper(
						getApplicationContext());
				boolean removed = db.delete(e);

				db.closeDB();

				class handleToast implements Runnable {
					boolean error;

					public handleToast(boolean error) {
						this.error = error;
					}

					@Override
					public void run() {
						if (error)
							Toast.makeText(
									getApplicationContext(),
									"Could not remove from Favorites, Try again.",
									Toast.LENGTH_SHORT).show();
						else
							Toast.makeText(getApplicationContext(),
									"Removed from Favorites",
									Toast.LENGTH_SHORT).show();
					}
				}
				;

				runOnUiThread(new handleToast(!removed));
			}
		});

		thread.start();
	}

}
