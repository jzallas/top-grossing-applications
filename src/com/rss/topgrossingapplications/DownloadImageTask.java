package com.rss.topgrossingapplications;

import java.io.InputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

class DownloadImageTask extends AsyncTask<Entry, Void, Bitmap> {
	ImageView imageView;

	public DownloadImageTask(ImageView imageView) {
		this.imageView = imageView;
	}

	protected Bitmap doInBackground(Entry... entry) {
		String urldisplay = entry[0].getIconUrls()[2];
		Bitmap icon = null;
		try {
			InputStream in = new java.net.URL(urldisplay).openStream();
			icon = BitmapFactory.decodeStream(in);
		} catch (Exception e) {
		}
		entry[0].setIcon(icon);
		return icon;
	}

	protected void onPostExecute(Bitmap result) {
		imageView.setImageBitmap(result);
	}
}