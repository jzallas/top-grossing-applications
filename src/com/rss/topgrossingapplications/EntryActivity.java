package com.rss.topgrossingapplications;


import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class EntryActivity extends Activity implements OnClickListener {

	TextView title, author, price, summary, releaseDate, category, rights;
	ImageView icon;
	Entry entry;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_entry);

		// Show the Up button in the action bar.
		setupActionBar();

		// get the entry that was pressed to start this activity
		entry = getIntent().getParcelableExtra(MainActivity.ENTRY_ITEM);

		// set the title of this activity to the entry's title
		setTitle(entry.getTitle());

		setText(entry);
	}

	/**
	 * Initialize the text in this activity with the appropriate data from the
	 * entry used to launch this activity
	 * 
	 * @param e
	 *            - the entry used to populate the TextViews
	 */
	private void setText(Entry e) {

		title = (TextView) findViewById(R.id.titleTextView);
		author = (TextView) findViewById(R.id.authorTextView);
		price = (TextView) findViewById(R.id.priceTextView);
		summary = (TextView) findViewById(R.id.summaryTextView);
		releaseDate = (TextView) findViewById(R.id.releaseDateTextView);
		category = (TextView) findViewById(R.id.categoryTextView);
		rights = (TextView) findViewById(R.id.rightsTextView);
		icon = (ImageView) findViewById(R.id.iconImageView);

		// click to access the links provided in the rss feed for these
		// respective data fields
		title.setOnClickListener(this);
		title.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
		category.setOnClickListener(this);
		category.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
		author.setOnClickListener(this);
		author.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);

		title.setText(e.getTitle());
		author.setText(e.getAuthor());
		price.setText(e.getCurrency() + " "
				+ String.format("%.2f", e.getPrice()));
		summary.setText(e.getSummary());
		releaseDate.setText(e.getReleaseDate());
		category.setText(e.getCategory());
		icon.setImageBitmap(e.getIcon());
		rights.setText(e.getRights());

	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.entry, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		case R.id.action_share:
			Intent i = new Intent(Intent.ACTION_SEND);
			i.setType("text/plain");
			i.putExtra(Intent.EXTRA_TEXT, getResources()
					.getText(R.string.check) + " : " + entry.getTitleLink());
			startActivity(Intent.createChooser(i,
					getResources().getText(R.string.sharewith)));
		case R.id.action_favorite:
			addFavorite();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Tries to add this Entry as a favorite (in a background thread)<br>
	 * Toast will appear saying whether or not it was added successfully
	 */
	private void addFavorite() {
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {

				FavoritesDBHelper db = new FavoritesDBHelper(
						getApplicationContext());
				boolean inserted = db.insert(entry);

				db.closeDB();

				class handleToast implements Runnable {
					boolean error;

					public handleToast(boolean error) {
						this.error = error;
					}

					@Override
					public void run() {
						if (error)
							Toast.makeText(getApplicationContext(),
									"Could not add to Favorites",
									Toast.LENGTH_SHORT).show();
						else
							Toast.makeText(getApplicationContext(),
									"Added to Favorites", Toast.LENGTH_SHORT).show();
					}
				}
				;

				runOnUiThread(new handleToast(!inserted));
			}
		});

		thread.start();
	}

	@Override
	public void onClick(View v) {
		Uri uri = null;
		switch (v.getId()) {
		case R.id.authorTextView:
			uri = Uri.parse(entry.getAuthorLink());
			break;
		case R.id.titleTextView:
			uri = Uri.parse(entry.getTitleLink());
			break;
		case R.id.categoryTextView:
			uri = Uri.parse(entry.getCategoryLink());
			break;
		default:
			break;
		}

		if (uri != null)
			startActivity(new Intent(Intent.ACTION_VIEW, uri));
	}

}
