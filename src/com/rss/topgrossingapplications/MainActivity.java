package com.rss.topgrossingapplications;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;
import android.widget.AdapterView.AdapterContextMenuInfo;

public class MainActivity extends Activity {

	public static final String ENTRY_ITEM = "MainActivity.EntryItem";
	GridView gridview;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);

		gridview = (GridView) findViewById(R.id.gridview);

		EntryAdapter adapter = new EntryAdapter(this);
		new HttpGETAsyncTask(adapter).execute();

		gridview.setAdapter(adapter);

		registerForContextMenu(gridview);

		gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				EntryAdapter adapter = (EntryAdapter) parent.getAdapter();
				Entry entry = (Entry) adapter.getItem(position);
				Intent i = new Intent(getApplicationContext(),
						EntryActivity.class);
				i.putExtra(ENTRY_ITEM, entry);
				startActivity(i);
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.favorites, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_launchFavs:
			startActivity(new Intent(this, FavoritesActivity.class));
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
		Entry entry = (Entry) gridview.getItemAtPosition(info.position);
		menu.setHeaderTitle(entry.getTitle());
		getMenuInflater().inflate(R.menu.context_menu, menu);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {

		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
				.getMenuInfo();
		Entry entry = (Entry) gridview.getItemAtPosition(info.position);

		switch (item.getItemId()) {
		case R.id.action_share:

			Intent i = new Intent(Intent.ACTION_SEND);
			i.setType("text/plain");
			i.putExtra(Intent.EXTRA_TEXT, getResources()
					.getText(R.string.check) + " : " + entry.getTitleLink());
			startActivity(Intent.createChooser(i,
					getResources().getText(R.string.sharewith)));
			return true;
		case R.id.action_favorite:
			addFavorite(entry);
		default:
			break;
		}

		return super.onContextItemSelected(item);
	}

	/**
	 * Tries to add a provided Entry as a favorite (in a background thread)<br>
	 * Toast will appear saying whether or not it was added successfully
	 */
	private void addFavorite(final Entry e) {
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {

				FavoritesDBHelper db = new FavoritesDBHelper(
						getApplicationContext());
				boolean inserted = db.insert(e);

				db.closeDB();

				class handleToast implements Runnable {
					boolean error;

					public handleToast(boolean error) {
						this.error = error;
					}

					@Override
					public void run() {
						if (error)
							Toast.makeText(getApplicationContext(),
									"Could not add to Favorites",
									Toast.LENGTH_SHORT).show();
						else
							Toast.makeText(getApplicationContext(),
									"Added to Favorites", Toast.LENGTH_SHORT)
									.show();
					}
				}
				;

				runOnUiThread(new handleToast(!inserted));
			}
		});

		thread.start();
	}
}
