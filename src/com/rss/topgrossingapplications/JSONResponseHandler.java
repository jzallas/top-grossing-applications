package com.rss.topgrossingapplications;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.impl.client.BasicResponseHandler;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONResponseHandler implements ResponseHandler<List<Entry>> {

	private static String TAG_FEED = "feed";

	private static String TAG_ENTRY = "entry";

	@Override
	public List<Entry> handleResponse(HttpResponse response)
			throws ClientProtocolException, IOException {

		String JSONResponse = new BasicResponseHandler()
				.handleResponse(response);
		List<Entry> entries = new ArrayList<Entry>();
		try {
			JSONObject result = new JSONObject(JSONResponse).getJSONObject(TAG_FEED);
			JSONArray JSONEntries = result.getJSONArray(TAG_ENTRY);
			for (int i = 0; i < JSONEntries.length(); i++){
				JSONObject thisEntry = (JSONObject) JSONEntries.get(i);
				entries.add(new Entry(thisEntry));
			}
			
		} catch (JSONException e) {
			// if the parse fails or doesn't yield a JSONObject.
		}
		return entries;
	}
}
