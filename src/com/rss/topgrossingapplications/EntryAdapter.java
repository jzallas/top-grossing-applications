package com.rss.topgrossingapplications;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class EntryAdapter extends BaseAdapter {
	private Context mContext;
	private List<Entry> entries = new ArrayList<Entry>();

	public EntryAdapter(Context c) {
		mContext = c;
	}

	public int getCount() {
		if (entries != null)
			return entries.size();
		else
			return 0;
	}

	// create a new ImageView for each item referenced by the Adapter
	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView imageView;
		if (convertView == null) { // if it's not recycled, initialize some
									// attributes
			imageView = new ImageView(mContext);
			imageView.setLayoutParams(new GridView.LayoutParams(100, 100));
			imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
			imageView.setPadding(8, 8, 8, 8);
		} else {
			imageView = (ImageView) convertView;
			// clear bitmap first because this view is being re-used and may
			// need to download the icon
			imageView.setImageBitmap(null);
		}

		Entry entry = (Entry) getItem(position);

		if (!entry.hasIcon()) // if theres no stored icon, download one
			new DownloadImageTask(imageView).execute(new Entry[] { entry });
		else {
			imageView.setImageBitmap(entry.getIcon());
		}

		return imageView;
	}

	public void setList(List<Entry> entries) {
		this.entries = entries;
	}

	/**
	 * @param position
	 *            - index of the item in its original structure
	 * @return - reference to item
	 */
	@Override
	public Object getItem(int position) {
		if (entries != null)
			if (position < entries.size())
				return entries.get(position);
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}
}