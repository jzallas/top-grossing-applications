package com.rss.topgrossingapplications;

import java.io.IOException;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;

import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.widget.BaseAdapter;

public class HttpGETAsyncTask extends AsyncTask<Void, Void, List<Entry>> {

	private static final String URL = "http://ax.itunes.apple.com/WebObjects/MZStoreServices.woa/ws/RSS/topgrossingapplications/sf=143441/limit=25/json";

	private AndroidHttpClient httpClient = AndroidHttpClient.newInstance("");
	private BaseAdapter adapter;

	public HttpGETAsyncTask(BaseAdapter adapter) {
		this.adapter = adapter;
	}

	@Override
	protected List<Entry> doInBackground(Void... params) {

		HttpGet request = new HttpGet(URL);

		JSONResponseHandler responseHandler = new JSONResponseHandler();

		try {
			return httpClient.execute(request, responseHandler);
		} catch (ClientProtocolException e) {
			// catch error in http protocol
		} catch (IOException e) {
			// catch generic connection issue
		} finally {
			if (httpClient != null)
				httpClient.close();
		}

		return null;
	}

	@Override
	protected void onPostExecute(List<Entry> result) {

		((EntryAdapter) adapter).setList(result);
		adapter.notifyDataSetChanged();
	}
}
