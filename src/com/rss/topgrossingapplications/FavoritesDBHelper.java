package com.rss.topgrossingapplications;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class FavoritesDBHelper extends SQLiteOpenHelper {

	// database version
	private static final int DATABASE_VERSION = 1;

	// database name
	private static final String DATABASE_NAME = "favorites_db";

	// table names
	private static final String TABLE_APPS = "apps";
	private static final String TABLE_AUTHORS = "authors";
	private static final String TABLE_CATEGORIES = "categories";

	// shared column names
	private static final String KEY_ID = "_id";

	// apps column names
	private static final String KEY_TITLE = "title";
	private static final String KEY_TITLE_LINK = "title_link";
	private static final String KEY_AUTHOR_ID = "author_id";
	private static final String KEY_PRICE = "price";
	private static final String KEY_CURRENCY = "currency";
	private static final String KEY_SUMMARY = "summary";
	private static final String KEY_RELEASE_DATE = "release_date";
	private static final String KEY_CATEGORY_ID = "category_id";
	private static final String KEY_RIGHTS = "rights";
	private static final String KEY_ICON = "icon";

	// category column names
	private static final String KEY_CATEGORY = "category";
	private static final String KEY_CATEGORY_LINK = "category_link";

	// author column names
	private static final String KEY_AUTHOR = "author";
	private static final String KEY_AUTHOR_LINK = "author_link";

	// table create statement
	private static final String FOREIGN_KEY_CATEGORY = "FOREIGN KEY("
			+ KEY_CATEGORY_ID + ") REFERENCES " + TABLE_CATEGORIES + "("
			+ KEY_ID + ")";
	private static final String FOREIGN_KEY_AUTHOR = "FOREIGN KEY("
			+ KEY_AUTHOR_ID + ") REFERENCES " + TABLE_AUTHORS + "(" + KEY_ID
			+ ")";
	private static final String CREATE_TABLE_APPS = "CREATE TABLE "
			+ TABLE_APPS + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
			+ KEY_TITLE + " TEXT," + KEY_TITLE_LINK + " TEXT UNIQUE,"
			+ KEY_PRICE + " REAL," + KEY_CURRENCY + " TEXT," + KEY_SUMMARY
			+ " TEXT," + KEY_RELEASE_DATE + " TEXT," + KEY_RIGHTS + " TEXT,"
			+ KEY_ICON + " BLOB," + KEY_AUTHOR_ID + " INTEGER,"
			+ KEY_CATEGORY_ID + " INTEGER," + FOREIGN_KEY_AUTHOR + ","
			+ FOREIGN_KEY_CATEGORY + ")";

	private static final String CREATE_TABLE_CATEGORIES = "CREATE TABLE "
			+ TABLE_CATEGORIES + "(" + KEY_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_CATEGORY + " TEXT,"
			+ KEY_CATEGORY_LINK + " TEXT UNIQUE)";
	private static final String CREATE_TABLE_AUTHORS = "CREATE TABLE "
			+ TABLE_AUTHORS + "(" + KEY_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_AUTHOR + " TEXT,"
			+ KEY_AUTHOR_LINK + " TEXT UNIQUE)";

	public FavoritesDBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE_AUTHORS);
		db.execSQL(CREATE_TABLE_CATEGORIES);
		db.execSQL(CREATE_TABLE_APPS);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_AUTHORS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORIES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_APPS);
		onCreate(db);
	}

	private long getAuthorID(String AuthorLink) {
		SQLiteDatabase db = getReadableDatabase();
		Cursor c = db.query(TABLE_AUTHORS, new String[] { KEY_ID },
				KEY_AUTHOR_LINK + "=?", new String[] { AuthorLink }, null,
				null, null);
		if (c.moveToFirst()) {
			long id = c.getInt(c.getColumnIndex(KEY_ID));
			c.close();
			return id;
		}
		return -1;
	}

	private long getCategoryID(String CategoryLink) {
		SQLiteDatabase db = getReadableDatabase();
		Cursor c = db.query(TABLE_CATEGORIES, new String[] { KEY_ID },
				KEY_CATEGORY_LINK + "=?", new String[] { CategoryLink }, null,
				null, null);
		if (c.moveToFirst()) {
			long id = c.getInt(c.getColumnIndex(KEY_ID));
			c.close();
			return id;
		}
		return -1;
	}

	/**
	 * insert an entry into the favorites database
	 * 
	 * @param e
	 *            - An entry to insert
	 * @return false if there was a problem inserting
	 */
	public boolean insert(Entry e) {
		SQLiteDatabase db = getWritableDatabase();

		ContentValues values = new ContentValues();

		// insert this author if it doesn't exist yet
		values.put(KEY_AUTHOR, e.getAuthor());
		values.put(KEY_AUTHOR_LINK, e.getAuthorLink());
		long authorID = -1;
		try {
			// if it doesnt exists, it will get an id here
			authorID = db.insertOrThrow(TABLE_AUTHORS, null, values);
		} catch (SQLException sqlException) {
			// if there was an error inserting, check to see if it does exist
			authorID = getAuthorID(values.getAsString(KEY_AUTHOR_LINK));
			// if it doesnt exist and it couldnt be added, stop insert
			if (authorID == -1)
				return false;
		}

		// insert this category if it doesn't exist yet
		values.clear();
		values.put(KEY_CATEGORY, e.getCategory());
		values.put(KEY_CATEGORY_LINK, e.getCategoryLink());
		long categoryID = -1;
		try {
			categoryID = db.insertOrThrow(TABLE_CATEGORIES, null, values);
		} catch (SQLException sqlException) {
			categoryID = getCategoryID(values.getAsString(KEY_CATEGORY_LINK));
			if (categoryID == -1)
				return false;
		}

		// insert this app if it doesn't already exist
		values.clear();
		values.put(KEY_CURRENCY, e.getCurrency());
		values.put(KEY_ICON, BitmapToByteArray(e.getIcon()));
		values.put(KEY_PRICE, e.getPrice());
		values.put(KEY_RELEASE_DATE, e.getReleaseDate());
		values.put(KEY_RIGHTS, e.getRights());
		values.put(KEY_SUMMARY, e.getSummary());
		values.put(KEY_TITLE, e.getTitle());
		values.put(KEY_TITLE_LINK, e.getTitleLink());
		values.put(KEY_AUTHOR_ID, authorID);
		values.put(KEY_CATEGORY_ID, categoryID);
		try {
			return (db.insertOrThrow(TABLE_APPS, null, values) != -1);
		} catch (SQLException sqlException) {
			return false;
		}

	}

	private byte[] BitmapToByteArray(Bitmap bmp) {
		if (bmp != null) {
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
			byte[] byteArray = stream.toByteArray();
			return byteArray;
		}
		return null;
	}

	private Bitmap ByteArrayToBitmap(byte[] src) {
		if (src == null)
			return null;
		return BitmapFactory.decodeByteArray(src, 0, src.length);
	}

	/**
	 * Removes an entry from the favorites database
	 * 
	 * @param e
	 *            - an entry to remove (the entry only needs to have a
	 *            titleLink)
	 * @return - true if removed successfully, false otherwise
	 */
	public boolean delete(Entry e) {
		SQLiteDatabase db = getWritableDatabase();
		return (0 != db.delete(TABLE_APPS, KEY_TITLE_LINK + "=?",
				new String[] { e.getTitleLink() }));
	}

	/**
	 * gets all the user saved favorites
	 * 
	 * @return an ArrayList of all the saved favorites
	 */
	public List<Entry> getFavorites() {
		SQLiteDatabase db = getReadableDatabase();

		List<Entry> entries = new ArrayList<Entry>();

		// SELECT * FROM apps a, authors b,categories c WHERE
		// c._id=a.category_id AND b._id=a.author_id;
		String select = "SELECT * FROM " + TABLE_APPS + " a, " + TABLE_AUTHORS
				+ " b," + TABLE_CATEGORIES + " c WHERE c." + KEY_ID + "=a."
				+ KEY_CATEGORY_ID + " AND b." + KEY_ID + "=a." + KEY_AUTHOR_ID
				+ ";";

		Cursor c = db.rawQuery(select, null);
		if (c.moveToFirst()) {
			do {
				String title = c.getString(c.getColumnIndex(KEY_TITLE));
				String rights = c.getString(c.getColumnIndex(KEY_RIGHTS));
				String author = c.getString(c.getColumnIndex(KEY_AUTHOR));
				String summary = c.getString(c.getColumnIndex(KEY_SUMMARY));
				String currency = c.getString(c.getColumnIndex(KEY_CURRENCY));
				String releaseDate = c.getString(c
						.getColumnIndex(KEY_RELEASE_DATE));
				String titleLink = c
						.getString(c.getColumnIndex(KEY_TITLE_LINK));
				String authorLink = c.getString(c
						.getColumnIndex(KEY_AUTHOR_LINK));
				Float price = c.getFloat(c.getColumnIndex(KEY_PRICE));
				String category = c.getString(c.getColumnIndex(KEY_CATEGORY));
				String categoryLink = c.getString(c
						.getColumnIndex(KEY_CATEGORY_LINK));
				Bitmap icon = ByteArrayToBitmap(c.getBlob(c
						.getColumnIndex(KEY_ICON)));

				Entry entry = new Entry(title, rights, author, summary,
						currency, category, categoryLink, releaseDate,
						titleLink, authorLink, price, icon);
				entries.add(entry);
			} while (c.moveToNext());
			c.close();
		}
		return entries;
	}

	/**
	 * closes the database after opening it
	 */
	public void closeDB() {
		SQLiteDatabase db = this.getReadableDatabase();
		if (db != null && db.isOpen())
			db.close();
	}
}
