package com.rss.topgrossingapplications;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Can be used to build entry objects which correspond to entry JSON objects
 * from the apple app store feed
 */
public class Entry implements Parcelable {

	private static String TAG_RIGHTS = "rights";
	private static String TAG_LINK = "link";
	private static String TAG_NAME = "im:name";
	private static String TAG_IMAGE = "im:image";
	private static String TAG_SUMMARY = "summary";
	private static String TAG_PRICE = "im:price";
	private static String TAG_ARTIST = "im:artist";
	private static String TAG_CATEGORY = "category";
	private static String TAG_RELEASE_DATE = "im:releaseDate";
	private static String TAG_LABEL = "label";
	private static String TAG_ATTRIBUTES = "attributes";
	private static String TAG_AMOUNT = "amount";
	private static String TAG_CURRENCY = "currency";
	private static String TAG_HREF = "href";
	private static String TAG_SCHEME = "scheme";

	private String title, rights, author, summary, currency, category,
			categoryLink, releaseDate, titleLink, authorLink;
	private String iconUrls[];
	private float price;

	private Bitmap icon = null;

	/**
	 * Creates an Entry if all the parsed data is available
	 * @param title
	 * @param rights
	 * @param author
	 * @param summary
	 * @param currency
	 * @param category
	 * @param categoryLink
	 * @param releaseDate
	 * @param titleLink
	 * @param authorLink
	 * @param price
	 * @param icon
	 */
	public Entry(String title, String rights, String author, String summary,
			String currency, String category, String categoryLink,
			String releaseDate, String titleLink, String authorLink, Float price, Bitmap icon) {
		this.title = title;
		this.rights = rights;
		this.author = author;
		this.summary = summary;
		this.currency = currency;
		this.category = category;
		this.categoryLink = categoryLink;
		this.releaseDate = releaseDate;
		this.titleLink = titleLink;
		this.authorLink = authorLink;
		this.price = price;
		this.icon = icon;

	}
	
	/**
	 * default constructor, creates an empty entry
	 */
	public Entry(){
		
	}

	/**
	 * Create an Entry object by parsing a JSON "entry" object
	 * 
	 * @param j
	 *            - an JSON Object retrieved from the tag "entry"
	 * @throws JSONException
	 *             - if the parse fails or doesn't yield a JSONObject.
	 */
	public Entry(JSONObject j) throws JSONException {

		setIconUrls(j.getJSONArray(TAG_IMAGE));
		setSummary(j.getJSONObject(TAG_SUMMARY));
		setPrice(j.getJSONObject(TAG_PRICE));
		setRights(j.getJSONObject(TAG_RIGHTS));
		setEntryLink(j.getJSONObject(TAG_LINK));
		setTitle(j.getJSONObject(TAG_NAME));
		setAuthor(j.getJSONObject(TAG_ARTIST));
		setCategory(j.getJSONObject(TAG_CATEGORY));
		setReleaseDate(j.getJSONObject(TAG_RELEASE_DATE));

	}

	/**
	 * @return - the title of this entry
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param jsonObject
	 *            the JSON Object containing the title to set
	 * @throws JSONException
	 */
	private void setTitle(JSONObject jsonObject) throws JSONException {
		this.title = jsonObject.getString(TAG_LABEL);
	}

	/**
	 * @return - the rights of this entry
	 */
	public String getRights() {
		return rights;
	}

	/**
	 * @param jsonObject
	 *            the JSON Object containing the rights to set
	 * @throws JSONException
	 */
	private void setRights(JSONObject jsonObject) throws JSONException {
		this.rights = jsonObject.getString(TAG_LABEL);
	}

	/**
	 * @return - the author or company that published this entry
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @param jsonObject
	 *            the JSON Object containing author/publisher to set
	 * @throws JSONException
	 */
	private void setAuthor(JSONObject jsonObject) throws JSONException {
		this.author = jsonObject.getString(TAG_LABEL);
		setAuthorLink(jsonObject.getJSONObject(TAG_ATTRIBUTES).getString(
				TAG_HREF));
	}

	/**
	 * @return - the summary for this entry
	 */
	public String getSummary() {
		return summary;
	}

	/**
	 * @param jsonObject
	 *            the JSON object containing a summary
	 * @throws JSONException
	 */
	private void setSummary(JSONObject jsonObject) throws JSONException {
		summary = jsonObject.getString(TAG_LABEL);
	}

	/**
	 * @return - the currency in which the price is listed for this entry
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency
	 *            the currency of the price to set
	 */
	private void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return - the category where this entry can be found
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param jsonObject
	 *            the JSON Object containing a category to set
	 * @throws JSONException
	 */
	private void setCategory(JSONObject jsonObject) throws JSONException {
		JSONObject j = jsonObject.getJSONObject(TAG_ATTRIBUTES);
		this.category = j.getString(TAG_LABEL);
		setCategoryLink(j.getString(TAG_SCHEME));
	}

	/**
	 * @return - A string containing the url which links to the category for
	 *         this entry
	 */
	public String getCategoryLink() {
		return categoryLink;
	}

	/**
	 * @param categoryLink
	 *            the categoryLink to set
	 */
	private void setCategoryLink(String categoryLink) {
		this.categoryLink = categoryLink;
	}

	/**
	 * @return - a String containing the release date
	 */
	public String getReleaseDate() {
		return releaseDate;
	}

	/**
	 * @param jsonObject
	 *            the JSONObject containing the releaseDate to set
	 * @throws JSONException
	 */
	private void setReleaseDate(JSONObject jsonObject) throws JSONException {
		this.releaseDate = jsonObject.getJSONObject(TAG_ATTRIBUTES).getString(
				TAG_LABEL);
	}

	/**
	 * @return - an array of Strings containing the urls associated with icon
	 *         sizes. The sizes are 53x53, 75x75, and 100x100
	 */
	public String[] getIconUrls() {
		return iconUrls;
	}

	/**
	 * @param jsonArray
	 *            the JSON array containing icon urls
	 * @throws JSONException
	 */
	private void setIconUrls(JSONArray jsonArray) throws JSONException {

		if (jsonArray.length() != 0) {
			iconUrls = new String[jsonArray.length()];
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject j = (JSONObject) jsonArray.get(i);
				iconUrls[i] = j.getString(TAG_LABEL);
			}
		}
	}

	/**
	 * @return - a float representing the price of this entry
	 */
	public float getPrice() {
		return price;
	}

	/**
	 * @param jsonObject
	 *            the JSON Object containing the price to set
	 * @throws JSONException
	 */
	private void setPrice(JSONObject jsonObject) throws JSONException {
		JSONObject j = jsonObject.getJSONObject(TAG_ATTRIBUTES);
		price = (float) j.getDouble(TAG_AMOUNT);
		setCurrency(j.getString(TAG_CURRENCY));
	}

	/**
	 * @return a String containing the URL which leads to this entry's page on
	 *         the app store
	 */
	public String getTitleLink() {
		return titleLink;
	}

	/**
	 * @param jsonObject
	 *            - the JSON object containing the link to this entry
	 * @throws JSONException
	 */
	private void setEntryLink(JSONObject jsonObject) throws JSONException {
		this.titleLink = jsonObject.getJSONObject(TAG_ATTRIBUTES).getString(
				TAG_HREF);
	}

	/**
	 * @return a String containing the URL which leads to the author/publisher's
	 *         page on the app store
	 */
	public String getAuthorLink() {
		return authorLink;
	}

	private void setAuthorLink(String authorLink) {
		this.authorLink = authorLink;
	}

	/**
	 * @return - returns stored icon for this entry or returns null if icon has
	 *         yet to be downloaded
	 */
	public Bitmap getIcon() {
		return icon;
	}

	/**
	 * @param icon
	 *            - a Bitmap to use as the icon
	 */
	public void setIcon(Bitmap icon) {
		this.icon = icon;
	}

	/**
	 * @return <em>true</em> if this entry has stored icon, <em>false</em> if it
	 *         doesn't
	 */
	public boolean hasIcon() {
		return (icon != null);
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(title);
		dest.writeString(rights);
		dest.writeString(author);
		dest.writeString(summary);
		dest.writeString(currency);
		dest.writeString(category);
		dest.writeString(categoryLink);
		dest.writeString(releaseDate);
		dest.writeString(titleLink);
		dest.writeString(authorLink);
		dest.writeFloat(price);
		if (icon != null)
			dest.writeParcelable(icon, 0);

	}
	
//	public Entry(String title, String rights, String author, String summary,
//			String currency, String category, String categoryLink,
//			String releaseDate, String titleLink, String authorLink, Bitmap icon) {

	public static final Parcelable.Creator<Entry> CREATOR = new Creator<Entry>() {
		public Entry createFromParcel(Parcel source) {
			Entry entry = new Entry();
			entry.title = source.readString();
			entry.rights = source.readString();
			entry.author = source.readString();
			entry.summary = source.readString();
			entry.currency = source.readString();
			entry.category = source.readString();
			entry.categoryLink = source.readString();
			entry.releaseDate = source.readString();
			entry.titleLink = source.readString();
			entry.authorLink = source.readString();
			entry.price = source.readFloat();
			try {
				entry.icon = source.readParcelable(null);
			} catch (Exception e) {
			}
			return entry;
		}

		public Entry[] newArray(int size) {
			return new Entry[size];
		}
	};

}
